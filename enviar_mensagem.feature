Feature: Enviar mensagem
Esta funcionalidade permite ao usuario enviar mensagens de texto

Scenario Outline: um usuario seleciona um contato para enviar mensagens de testo

  Given Eu devo estar na pagina de conversas do Whatsapp
  When Eu seleciono no botao de nova conversa
  Then Minha lista de contatos eh exibida
  When Eu seleciono em um <contato>
  Then A pagina de mensagem eh aberta
  And Uma caixa de texto eh exibida
  When Eu escrevo uma <mensagem> de texto
  And Envio a <mensagem>
  Then Um tick aparece ao lado da <mensagem> enviada


Examples: 

  | contato  | mensagem                      |
  |  "Joao"  |  "Ola, tudo bem?"             |
  |  "Maria" |  "Pode falar?"                |
  |  "Jose"  |  "Quando puder me liga. Bjs" |