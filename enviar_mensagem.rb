Given ("Eu devo estar na pagina de conversas do Whatsapp") do
  puts "Estou na pagina de conversas"
end

When ("Eu seleciono no botao de nova conversa") do
  puts "Nova conversa iniciada"
end

Then ("Minha lista de contatos eh exibida") do
  puts "Lista de contatso exibida"
end

When ("Eu seleciono em um $x") do |contato|
  puts "Contato #{contato} selecionado"
end

Then ("A pagina de mensagem eh aberta") do
  puts "Estou na pagina de mensagem"
end

And ("Uma caixa de texto eh exibida") do
  puts "Caixa de texto exibida"
end

When ("Eu escrevo uma $x de texto") do |mensagem|
  puts "Mensagem #{mensagem} digiatda na caixa de texto"
end

And ("Envio a $x") do |mensagem|
  puts "Mensagem #{mensagem} enviada"
end

Then ("Um tick aparece ao lado da $x enviada") do |mensagem|
  puts "Mensagem enviada"
end