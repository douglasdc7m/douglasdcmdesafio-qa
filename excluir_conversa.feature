Feature: Excluir conversa
Esta funcionalidade permite ao usuario excluir conversas

Scenario Outline: um usuario exclui uma conversa da lista

  Given Eu deva estar na minha lista de contatos do Whatsapp
  When Eu pressiono uma <conversa> por alguns segundos
  Then O icone de lixeira eh exibido
  And A <conversa> eh marcada com um tick
  When Eu seleciono o icone de lixeira
  And Eu confirmo a exclusao da <conversa>
  Then A <conversa> eh elimina da lista

Examples: 

  | conversa  |
  |  "Joao"   |
  |  "Maria"  |
  |  "Jose"   |